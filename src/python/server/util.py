import os
import subprocess
import socket

def detect_ip():
	return socket.gethostbyname(socket.gethostname())

def auto_port(addr, start):
	pp = start
	while pp < 65535:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		con = s.connect_ex((addr, pp))
		s.close()
		if con != 0:
			break
		pp = pp + 1
	return pp

def escp2html(inFileName, outFileName):
	sedFileName = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'escp-to-html.sed')
	outFile = open(outFileName, 'w')
	outFile.write('<html><body style="white-space:pre; font-family:monospace">')
	outFile.flush()
	sub = subprocess.call(['sed', '-f', sedFileName, inFileName], stdout=outFile)
	outFile.flush()
	outFile.write('</body></html>')
	outFile.close()

