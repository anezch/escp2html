import sys
import os
import tempfile
from . import util
from flask import Flask, Response

SV_HOST = '0.0.0.0'  ## util.detect_ip()
SV_PORT = util.auto_port(SV_HOST, 5000)

workFileName = ''
escpFileName = '%s/.dosemu/drive_c/kor2008i/print.out' % os.environ['HOME']

app = Flask(__name__)

@app.route("/")
def serve():
	util.escp2html(escpFileName, workFileName)
	return Response(open(workFileName).read(), mimetype='text/html')

@app.route('/reset')
def serve_reset():
	try:
		f = open(escpFileName, 'w')
		f.write('')
		f.close()
	except:
		return 'Failed'
	return 'OK'

if __name__ == "__main__":
	if len(sys.argv) > 1:
		escpFileName = sys.argv[1]

	workFd, workFileName = tempfile.mkstemp(prefix='eh-%s-' % os.environ['USER'])
	os.close(workFd)

	app.run(host=SV_HOST, port=SV_PORT)
