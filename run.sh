#!/bin/bash

mypath="$(dirname "`readlink -f "$0"`")"

cd $mypath/src/python

PYTHON2=$(which python2)
if [ -z "$PYTHON2" ]; then
	PYTHON2=$(which python)
fi

$PYTHON2 -m server.server $@

