ESCP2HTML
=========

## Requirements

- Python 2.7
- Flask

## Running

To run:

	./run.sh <path-to-print-file>

The http server will listen on port 5000
